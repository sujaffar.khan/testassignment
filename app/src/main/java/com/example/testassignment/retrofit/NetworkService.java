package com.example.testassignment.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NetworkService {



    static InternetConnectionListener internetConnectionListener;

    public interface InternetConnectionListener {
        void onInternetUnavailable();
    }

    public static  void setInternetConnectionListener(InternetConnectionListener listener) {
        internetConnectionListener = listener;
    }
    private static OkHttpClient httpClient;


    static {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();

                       /* if (response.code() == 401)
                            System.out.println("SESSION_EXPIRED");*/
                        return chain.proceed(request);
                    }
                })
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .build();


    }
    private static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            .setPrettyPrinting() // Pretty print
            .create();


    public static <S> S createService(Class<S> serviceClass) {
        System.out.println("time out");
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl("https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/")
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
