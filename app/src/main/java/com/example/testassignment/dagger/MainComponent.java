package com.example.testassignment.dagger;

import com.example.testassignment.MainApplication;
import com.example.testassignment.activity.MainActivity;
import com.example.testassignment.api.JsonFeedService;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {MainModule.class})
@Singleton
public interface MainComponent {
    void inject(MainApplication mainApplication);
    void inject(MainActivity mainActivity);
    void inject(JsonFeedService jsonFeedService);
}
