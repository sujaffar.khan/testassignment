package com.example.testassignment.dagger;


import android.content.Context;

import com.example.testassignment.MainApplication;
import com.example.testassignment.api.RestApi;
import com.example.testassignment.retrofit.NetworkService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    @Provides
    @Singleton
    Context provideContext() {
        return MainApplication.getInstance().getApplicationContext();
    }

    @Provides
    @Singleton
    RestApi provideApi() {
        return NetworkService.createService(RestApi.class);
    }
}
