package com.example.testassignment.api;

import android.content.Context;

import com.example.testassignment.MainApplication;
import com.example.testassignment.R;
import com.example.testassignment.model.CanadaList;
import com.example.testassignment.utils.Utils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JsonFeedService {

    @Inject
    RestApi restApi;

    private Context context;

    private  ApiListener listener;
    public interface ApiListener{
        void onSuccess(CanadaList canadaList,String header_title);
        void onFailure(String error);
    }

    public JsonFeedService(Context context, ApiListener listener) {
        this.context = context;
        MainApplication.getInstance().getMainComponent().inject(this);
        this.listener = listener;
    }

    public JsonFeedService() {
    }

    public void callApi() {
        if (Utils.isNetworkAvailable()){
            Call<ResponseBody> call = restApi.getCanadaList();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {

                        if (response.code()==200){

                            try {
                                String responseString = response.body().string();
                                System.out.println("canada list====>"+responseString);
                                JSONObject jsonObject=new JSONObject(responseString);
                                String title=jsonObject.getString("title");


                                // parsing json
                                ObjectMapper mapper = new ObjectMapper();
                                mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
                                JsonNode node = mapper.readTree(responseString);
                                CanadaList canadaList =mapper.readValue(node.get("rows").toString(), CanadaList.class);


                                if (listener!=null)
                                    listener.onSuccess(canadaList,title);

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                        else {
                            // error
                            if (listener!=null)
                                listener.onFailure(context.getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (listener!=null)
                        listener.onFailure(context.getResources().getString(R.string.server_error));
                }
            });
        }
        else {
            if (listener!=null)
                listener.onFailure(context.getResources().getString(R.string.network_neded_info));
        }


    }

}
