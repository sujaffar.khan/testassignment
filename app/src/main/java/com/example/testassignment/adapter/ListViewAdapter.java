package com.example.testassignment.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.testassignment.R;
import com.example.testassignment.model.CanadaList;

public class ListViewAdapter extends BaseAdapter {

    private Context mContext;
    CanadaList canada_list;
    private LayoutInflater mLayoutInflater;

    public ListViewAdapter(CanadaList canada_list,Context mContext) {
        this.mContext = mContext;
        this.canada_list = canada_list;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return canada_list.size();
    }

    @Override
    public Object getItem(int i) {
        return canada_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        RelativeLayout itemView;
        if (convertView == null) {
            itemView = (RelativeLayout) mLayoutInflater.inflate(
                    R.layout.custom_row_item, parent, false);

        } else {
            itemView = (RelativeLayout) convertView;
        }
        TextView title=itemView.findViewById(R.id.title);
        TextView description=itemView.findViewById(R.id.description);
        ImageView image=itemView.findViewById(R.id.image);
        if (canada_list.get(i).getTitle()!=null && !canada_list.get(i).getTitle().isEmpty() && !canada_list.get(i).getTitle().equalsIgnoreCase("null")){
            title.setText(canada_list.get(i).getTitle());

        }
        else {
            title.setText("");

        }
        if (canada_list.get(i).getDescription()!=null && !canada_list.get(i).getDescription().isEmpty() && !canada_list.get(i).getDescription().equalsIgnoreCase("null")){
            description.setText(canada_list.get(i).getDescription());

        }
        else {
            description.setText("");

        }




        if (canada_list.get(i).getImageHref()!=null && !canada_list.get(i).getImageHref().isEmpty() && !canada_list.get(i).getImageHref().equalsIgnoreCase("null")){
            Glide.with(mContext)
                    .load(canada_list.get(i).getImageHref())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                            return false;
                        }
                    })
                    .into(image);
        }



        return itemView;
    }
}
