package com.example.testassignment.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.testassignment.R;
import com.example.testassignment.model.CanadaList;


import java.util.List;

public class CanadaListAdapter extends RecyclerView.Adapter<CanadaListAdapter.ViewHolder> {

    CanadaList canada_list;
    Context context;
    public CanadaListAdapter(CanadaList canada_list, Context context) {
        this.canada_list = canada_list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_row_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.title.setText(canada_list.get(i).getTitle());
        viewHolder.description.setText(canada_list.get(i).getDescription());
        Glide.with(context)
                .load(canada_list.get(i).getImageHref())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        return false;
                    }
                })
                .into(viewHolder.image);


    }

    @Override
    public int getItemCount() {
        return canada_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView title,description;
        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            description=itemView.findViewById(R.id.description);
            image=itemView.findViewById(R.id.image);
        }
    }
}

