package com.example.testassignment.unittest;

import com.example.testassignment.model.CanadaList;

public interface MainActivityView {
     boolean isNetworkAvailable();

     void showNoNetworkMessage(int network_neded_info);

     CanadaList getCanadaList();

    void showMessageForListNotEmpty(int have_data);

    void showMessageForListEmpty(int no_data);
}
