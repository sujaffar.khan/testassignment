package com.example.testassignment.unittest;

import com.example.testassignment.R;
import com.example.testassignment.api.JsonFeedService;
import com.example.testassignment.model.CanadaList;

public class JsonFeedRepresenter {
    private MainActivityView view;
    private JsonFeedService jsonFeedService;

    public JsonFeedRepresenter(MainActivityView view, JsonFeedService jsonFeedService) {
        this.view = view;
        this.jsonFeedService = jsonFeedService;
    }

    public void onStartedApiCall() {
        boolean isAvailableNetwork=view.isNetworkAvailable();
        if (!isAvailableNetwork){
            view.showNoNetworkMessage(R.string.network_neded_info);
        }

    }

    public void haveItemInList(){
        CanadaList canadaList=view.getCanadaList();
        if (canadaList!=null && canadaList.size()>0){
            view.showMessageForListNotEmpty(R.string.have_data);
        }
    }

    public void noItemInList() {
        CanadaList canadaList=view.getCanadaList();
        if (canadaList==null || canadaList.size()==0){
            view.showMessageForListEmpty(R.string.no_data);
        }
    }
}
