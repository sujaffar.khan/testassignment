package com.example.testassignment.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.testassignment.MainApplication;
import com.example.testassignment.R;
import com.example.testassignment.adapter.CanadaListAdapter;
import com.example.testassignment.adapter.ListViewAdapter;
import com.example.testassignment.api.JsonFeedService;
import com.example.testassignment.api.RestApi;
import com.example.testassignment.model.CanadaList;
import com.example.testassignment.unittest.JsonFeedRepresenter;
import com.example.testassignment.unittest.MainActivityView;
import com.example.testassignment.utils.Utils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainActivityView {
    @Inject
    RestApi restApi;

    @BindView(R.id.main_title)
    TextView main_title;
    @BindView(R.id.no_internet_view)
    RelativeLayout no_internet_view;
    @BindView(R.id.no_data_view)
    RelativeLayout no_data_view;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;
//    @BindView(R.id.recycler_view)
//    RecyclerView recycler_view;
    CanadaListAdapter canadaListAdapter;
    @BindView(R.id.list_view)
    ListView list_view;
    ListAdapter listAdapter;

    private JsonFeedRepresenter jsonFeedRepresenter;

    CanadaList canadaList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getMainComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);




        // setting layout manager and fixed size for better performance
        //recycler_view.setHasFixedSize(true);
        //recycler_view.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        // initialize re presenter for unit test case
        jsonFeedRepresenter=new JsonFeedRepresenter(this,new JsonFeedService());

        doApicall();

        // swipe refresh listener
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                 doApicall();

            }
        });




        // this piece of code is done for swipe refresh otherwise scrolling is not getting properly

        list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list_view.getChildAt(0) != null) {
                    swipe_refresh.setEnabled(list_view.getFirstVisiblePosition() == 0 && list_view.getChildAt(0).getTop() == 0);
                }
            }
        });



    }

    private void doApicall() {

        jsonFeedRepresenter.onStartedApiCall();


        // JsonFeedService class is responsible for api call and returning the main list data if available

        new JsonFeedService(getApplicationContext(), new JsonFeedService.ApiListener() {
            @Override
            public void onSuccess(CanadaList canadaLists,String header_title) {
                try {
                    if (canadaList!=null){
                        canadaList.clear();
                    }
                    else {
                        canadaList=new CanadaList();
                    }

                    if (swipe_refresh.isRefreshing()){
                        swipe_refresh.setRefreshing(false);
                    }

                    canadaList.addAll(canadaLists);
                    main_title.setText(header_title);

                    // if list is null or empty

                    if (canadaList!=null && canadaList.size()>0){
                        //canadaListAdapter=new CanadaListAdapter(canadaList,MainActivity.this);
                        listAdapter=new ListViewAdapter(canadaList,MainActivity.this);
                        //recycler_view.setAdapter(listAdapter);
                        list_view.setAdapter(listAdapter);
                        doVisibiltyCheck(canadaList,"");
                        jsonFeedRepresenter.haveItemInList();

                    }
                    else {
                      doVisibiltyCheck(canadaList,getString(R.string.no_data));
                      jsonFeedRepresenter.noItemInList();
                    }

                }
                catch (Exception e){
                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(String error) {

                Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
                doVisibiltyCheck(null,error);

            }
        }).callApi();

    }


    // doing visibility checking as per available data.

    private void doVisibiltyCheck(CanadaList canadaList,String msg){
        if (swipe_refresh.isRefreshing()){
            swipe_refresh.setRefreshing(false);
        }
        if (canadaList!=null && canadaList.size()>0){
            //recycler_view.setVisibility(View.VISIBLE);
            list_view.setVisibility(View.VISIBLE);
            no_internet_view.setVisibility(View.GONE);
            no_data_view.setVisibility(View.GONE);
            return;
        }



        if (canadaList==null && !msg.isEmpty()){
            if (msg.equalsIgnoreCase(getString(R.string.network_neded_info))){
                //recycler_view.setVisibility(View.GONE);
                list_view.setVisibility(View.GONE);
                no_internet_view.setVisibility(View.VISIBLE);
                no_data_view.setVisibility(View.GONE);
            }

            return;

        }

        if (!msg.isEmpty()){
            if (msg.equalsIgnoreCase(getString(R.string.no_data))){
                //recycler_view.setVisibility(View.GONE);
                list_view.setVisibility(View.GONE);
                no_internet_view.setVisibility(View.GONE);
                no_data_view.setVisibility(View.VISIBLE);
            }

            return;
        }

    }

    @Override
    public boolean isNetworkAvailable() {
        return Utils.isNetworkAvailable();
    }

    @Override
    public void showNoNetworkMessage(int network_neded_info) {
        doVisibiltyCheck(null,getString(network_neded_info));
    }

    @Override
    public CanadaList getCanadaList() {
        return canadaList;
    }

    @Override
    public void showMessageForListNotEmpty(int have_data) {
        doVisibiltyCheck(canadaList,"");
        Toast.makeText(this, getString(have_data), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showMessageForListEmpty(int no_data) {
        doVisibiltyCheck(canadaList,getString(R.string.no_data));
        Toast.makeText(this, getString(no_data), Toast.LENGTH_SHORT).show();
    }
}
