package com.example.testassignment;

import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.example.testassignment.dagger.DaggerMainComponent;
import com.example.testassignment.dagger.MainComponent;

public class MainApplication extends MultiDexApplication {
    private static MainApplication mInstance;
    private MainComponent mainComponent;

    public MainComponent getMainComponent() {
        return mainComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mainComponent = DaggerMainComponent.builder().build();
        mainComponent.inject(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static MainApplication getInstance() {
        return mInstance;
    }

}
