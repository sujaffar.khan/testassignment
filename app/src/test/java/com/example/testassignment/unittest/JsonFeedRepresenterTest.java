package com.example.testassignment.unittest;

import com.example.testassignment.R;
import com.example.testassignment.api.JsonFeedService;
import com.example.testassignment.model.CanadaList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JsonFeedRepresenterTest {
    @Mock
    private MainActivityView view;
    @Mock
    private JsonFeedService service;
    private JsonFeedRepresenter jsonFeedRepresenter;

    @Before
    public void setUp() throws Exception {
        jsonFeedRepresenter = new JsonFeedRepresenter(view, service);
    }

    @Test
    public void showErrorMsgWhenNoInternetConnection() throws Exception {
        when(view.isNetworkAvailable()).thenReturn(false);
        jsonFeedRepresenter.onStartedApiCall();

        verify(view).showNoNetworkMessage(R.string.network_neded_info);
    }

    @Test
    public void showMessageWhenAlldataIsPresent() throws Exception {
        when(view.getCanadaList()).thenReturn(new CanadaList());
        jsonFeedRepresenter.haveItemInList();

        verify(view).showMessageForListNotEmpty(R.string.have_data);
    }

    @Test
    public void showMessageWhenNoDataAvailable() throws Exception {
        when(view.getCanadaList()).thenReturn(new CanadaList());
        jsonFeedRepresenter.noItemInList();

        verify(view).showMessageForListEmpty(R.string.no_data);
    }




}